# emup_ttu_bot
Неофициальный Telegram-бот для информирования о движении трамваев и троллейбусов Екатеринбурга. Обёртка над сайтом [http://m.ettu.ru](http://m.ettu.ru).

Данная программа не имеет никакого отношения к ЕМУП ТТУ и самим данным о движении городского транспорта, а лишь предоставляет эти данные в удобной для пользователя форме.

## Требования
- Python 3.9

## Установка и запуск
1. Установите необходимые зависимости
```sh
git clone https://codeberg.org/mkrnv/emup_ttu_bot.git
cd emup_ttu_bot
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```
2. Создайте файл конфигурации в корне проекта и укажите в нём ключ Telegram Bot API
```sh
touch config.ini
```
```ini
[Telegram]
API_Key='*YOUR_API_KEY*'
```
3. Запустите бота
```
python start.py
```

## Лицензия
MIT
