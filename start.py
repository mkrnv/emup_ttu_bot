from configparser import ConfigParser
from emup_ttu_bot.bot import EmupTtuBot

from emup_ttu_bot.logging.setup import setup_loggers

if __name__ == '__main__':
    config = ConfigParser()
    config.read('config.ini')

    setup_loggers()

    bot = EmupTtuBot(config['Telegram']['API_Key'])
    bot.start()
