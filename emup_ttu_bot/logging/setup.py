import sqlite3
import logging

from .handlers import UserActionDbHandler, ScheduleCheckDbHandler


def setup_loggers():
    connection = sqlite3.connect('bot.db')
    cursor = connection.cursor()
    cursor.execute("".join([
        'CREATE TABLE IF NOT EXISTS ',
        'actions(',
        'id INTEGER PRIMARY KEY, '
        'user_id VARCHAR, ',
        'module VARCHAR, ',
        'action VARCHAR, '
        'timestamp VARCHAR)']))
    cursor.execute("".join([
        'CREATE TABLE IF NOT EXISTS ',
        'schedule_checks(',
        'id INTEGER PRIMARY KEY, ',
        'station_id VARCHAR, ',
        'timestamp VARCHAR)']))

    actions_logger = logging.getLogger('emup_ttu_bot.bot')
    actions_handler = UserActionDbHandler()
    actions_handler.setFormatter(logging.Formatter(";".join([
        '%(message)s',
        '%(name)s',
        '%(funcName)s',
        '%(asctime)s'])))
    actions_logger.addHandler(actions_handler)
    actions_logger.setLevel(logging.INFO)

    schedule_logger = logging.getLogger('schedule_checks')
    schedule_handler = ScheduleCheckDbHandler()
    schedule_handler.setFormatter(logging.Formatter(';'.join([
        '%(message)s',
        '%(asctime)s'])))
    schedule_logger.addHandler(schedule_handler)
    schedule_logger.setLevel(logging.INFO)
