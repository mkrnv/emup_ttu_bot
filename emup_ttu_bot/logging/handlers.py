import logging
import sqlite3


class UserActionDbHandler(logging.Handler):
    def emit(self, record):
        msg = self.format(record)
        log_parts = tuple(msg.split(';'))
        with sqlite3.connect('bot.db') as con:
            con.execute("".join([
                'INSERT INTO actions(user_id, module, action, timestamp) ',
                'VALUES(?, ?, ?, ?)']), log_parts)


class ScheduleCheckDbHandler(logging.Handler):
    def emit(self, record):
        msg = self.format(record)
        log_parts = tuple(msg.split(';'))
        with sqlite3.connect('bot.db') as con:
            con.execute("".join([
                'INSERT INTO schedule_checks(station_id, timestamp) ',
                'VALUES(?, ?)']), log_parts)
