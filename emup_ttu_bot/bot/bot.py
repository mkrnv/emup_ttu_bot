from .handlers.schedule import show_schedule, update_schedule
from .handlers.stations import list_stations

from telegram import Update
from telegram.ext import (filters,
                          ApplicationBuilder, MessageHandler,
                          CommandHandler, CallbackQueryHandler,
                          CallbackContext)

import re


async def start_helper(update: Update, context: CallbackContext.DEFAULT_TYPE):
    message = 'Отправь первую букву, слово или название остановки, '\
            + 'и я найду подходящие варианты'
    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text=message)


class EmupTtuBot:
    def __init__(self, api_key: str):
        self.api_key = api_key

    def start(self):
        application = ApplicationBuilder().token(self.api_key).build()

        start_helper_handler = CommandHandler('start', start_helper)
        list_stations_handler = MessageHandler(filters.TEXT,
                                               list_stations)
        show_schedule_handler = CallbackQueryHandler(show_schedule,
                                                     re.compile(r'^(tm|tl)\+'))
        update_schedule_handler = CallbackQueryHandler(update_schedule,
                                                       re.compile(r'^(upd)\+'))

        application.add_handler(start_helper_handler)
        application.add_handler(list_stations_handler)
        application.add_handler(show_schedule_handler)
        application.add_handler(update_schedule_handler)

        application.run_polling()
