def _construct_schedule_buttons(data: str):
    return [
        [{
            'text': 'Обновить',
            'callback_data': 'upd+' + data
        }]
    ]


def _screen_message(message: str):
    return message.replace('(', '\\(')\
                  .replace(')', '\\)')\
                  .replace('-', '\\-')\
                  .replace('.', '\\.')\
                  .replace(',', '\\,')


def _render_station_title(identifier: str):
    if identifier == 'tm':
        return u'🚃 Трамвайная остановка *"{}"*'
    elif identifier == 'tl':
        return u'🚎 Троллейбусная остановка *"{}"*'


def _render_station(identifier: str, title: str, schedule: tuple):
    message = _render_station_title(identifier).format(title)
    if len(schedule) > 0:
        message += '\n\nМаршруты:\n\n'
        for entry in schedule:
            message += '*№{}*, через {} мин ({} м)\n'.format(entry[0],
                                                             entry[1],
                                                             entry[2])
    else:
        message += '\n\n*Нет данных.*'
    return message
