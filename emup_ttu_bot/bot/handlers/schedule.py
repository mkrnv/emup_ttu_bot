from emup_ttu_bot.wrapper import ETTUWrapper

from ..helpers import (_construct_schedule_buttons,
                       _render_station,
                       _screen_message)

from telegram import Update
from telegram.ext import CallbackContext

import logging


async def show_schedule(update: Update,
                        context: CallbackContext.DEFAULT_TYPE):
    wrapper = ETTUWrapper()
    identifier, link = update.callback_query.data.split('+')
    schedule, title = wrapper.get_schedule_by_url(link)

    message = _render_station(identifier, title, schedule)

    buttons = [
        [{
            'text': 'Обновить',
            'callback_data': 'upd+' + update.callback_query.data
        }]
    ]

    logging.getLogger(__name__).info(update.effective_chat.username)
    logging.getLogger('schedule_checks').info(link)

    await context.bot.edit_message_text(
            chat_id=update.effective_chat.id,
            message_id=update.effective_message.message_id,
            text=_screen_message(message),
            parse_mode='MarkdownV2',
            reply_markup={
                'inline_keyboard': buttons})


async def update_schedule(update: Update,
                          context: CallbackContext.DEFAULT_TYPE):
    wrapper = ETTUWrapper()
    _, identifier, link = update.callback_query.data.split('+')

    await context.bot.edit_message_text(
            chat_id=update.effective_chat.id,
            message_id=update.effective_message.message_id,
            text=u'♻️ Обновляю...')

    schedule, title = wrapper.get_schedule_by_url(link)

    message = _render_station(identifier, title, schedule)
    reply_markup = {
        'inline_keyboard': _construct_schedule_buttons('+'.join([identifier,
                                                                 link]))}

    logging.getLogger(__name__).info(update.effective_chat.username)
    logging.getLogger('schedule_checks').info(link)

    await context.bot.edit_message_text(
            chat_id=update.effective_chat.id,
            message_id=update.effective_message.message_id,
            text=_screen_message(message),
            parse_mode='MarkdownV2',
            reply_markup=reply_markup)
