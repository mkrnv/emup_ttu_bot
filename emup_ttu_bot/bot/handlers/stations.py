from emup_ttu_bot.wrapper import ETTUWrapper

from ..helpers import _screen_message

from telegram import Update
from telegram.ext import CallbackContext

import logging


async def list_stations(update: Update, context: CallbackContext.DEFAULT_TYPE):
    wrapper = ETTUWrapper()
    results = wrapper.search_station(update.message.text)
    buttons = []
    for result in results['Трамваи']:
        buttons.append([{'text': u'🚃 ' + result[0],
                         'callback_data': '+'.join(['tm',
                                                    result[1]])}])
    for result in results['Троллейбусы']:
        buttons.append([{'text': u'🚎 ' + result[0],
                         'callback_data': '+'.join(['tl',
                                                   result[1]])}])

    if len(buttons) == 0:
        message = 'По запросу *' + update.message.text + '* ничего не нашлось'
    else:
        if len(update.message.text) == 1:
            message = 'Все остановки на букву *' +\
                      update.message.text.upper() +\
                      '*:'
        else:
            message = 'Вот, что я нашёл по запросу *' +\
                      update.message.text +\
                      '*:'

        if len(buttons) > 0:
            message += '\n\nУсловные обозначения:\n'
            message += u'🚃 Трамваи\n'
            message += u'🚎 Троллейбусы'

    logging.getLogger(__name__).info(update.effective_chat.username)

    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text=_screen_message(message),
                                   parse_mode='MarkdownV2',
                                   reply_markup={
                                        'inline_keyboard': buttons})
