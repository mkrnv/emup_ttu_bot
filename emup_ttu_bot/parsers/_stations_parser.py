from html.parser import HTMLParser

TRAMS = 'Трамваи'
TROLLEYS = 'Троллейбусы'
REPORT_LABEL = 'Замечания по сервису'


class StationsParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self._clean()

    def _clean(self):
        self.last_tag = ''
        self.last_link = ''

        self.next_is_tram = False
        self.next_is_trolley = False

        self.trams = []
        self.trolleys = []

    def feed(self, feed):
        self._clean()
        cleaned_feed = feed.replace('\r', '').replace('\n', '')
        super().feed(cleaned_feed)
        return {
            TRAMS: tuple(self.trams),
            TROLLEYS: tuple(self.trolleys)  # this [:-3] should be fixed
        }

    def handle_starttag(self, tag, attrs):
        self.last_tag = tag
        if len(attrs) > 0:
            try:
                self.last_link = next(
                                    filter(
                                        lambda attr: attr[0] == 'href',
                                        attrs))[1]
            except StopIteration:
                pass

    def handle_data(self, data):
        if self.last_tag == 'h3':
            if data == TRAMS:
                self.next_is_tram = True
                self.next_is_trolley = False
            elif data == TROLLEYS:
                self.next_is_trolley = True
                self.next_is_tram = False

        if data == REPORT_LABEL:
            self.next_is_tram = False
            self.next_is_trolley = False
            return

        if self.last_tag == 'a' and self.last_link:
            if self.next_is_tram:
                self.trams.append((data, self.last_link,))
            elif self.next_is_trolley:
                self.trolleys.append((data, self.last_link,))
