from html.parser import HTMLParser

ROUTE_CONTAINER_STYLE = 'width: 3em;display:inline-block;text-align:center;'
TIME_CONTAINER_STYLE = 'width: 4em;display:inline-block;text-align:right;'
DISTANCE_CONTAINER_STYLE = 'width: 5em;display:inline-block;text-align:right;'


class ScheduleParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.last_tag = ''
        self.last_tag_style = ''

        self.station_title = ''
        self.last_route = -1
        self.last_time = -1
        self.last_distance = -1

        self.next_tag_will_be_route = False
        self.next_is_title = True

    def feed(self, feed: str):
        self.schedule = []
        self.station_title = ''
        cleaned_feed = feed.replace('\r', '')\
                           .replace('\n', '')\
                           .replace('  ', '')
        super().feed(cleaned_feed)
        # self.schedule = ((32, 2, 504,), (33, 6, 1388,),) # for testing purposes
        return (tuple(self.schedule), self.station_title,)

    def handle_starttag(self, tag, attrs):
        self.last_tag = tag

        if len(attrs) > 0:
            try:
                self.last_tag_style = next(
                                        filter(
                                            lambda attr: attr[0] == 'style',
                                            attrs))[1]
            except StopIteration:
                pass

    def handle_data(self, data):
        if data == ' ':
            return

        if self.next_tag_will_be_route:
            self.next_tag_will_be_route = False
            self.last_route = int(data)
            return

        if self.last_tag == 'p' and self.next_is_title:
            self.next_is_title = False
            self.station_title = ', '.join(data.split(', ')[0:-1])

        if (self.last_tag == 'b'
                and self.last_tag_style == ROUTE_CONTAINER_STYLE):
            # e.g. '4'
            self.last_route = int(data)

        elif (self.last_tag == 'div'
                and self.last_tag_style == TIME_CONTAINER_STYLE):
            # e.g. '15 мин'
            self.last_time = int(data.split(' ')[0])
            self.next_tag_is_whitespace = True

        elif (self.last_tag == 'div'
                and self.last_tag_style == DISTANCE_CONTAINER_STYLE):
            # e.g. '400 м'
            self.last_distance = int(data.split(' ')[0])
            # time to save!
            self.schedule.append((self.last_route,
                                  self.last_time,
                                  self.last_distance,))
