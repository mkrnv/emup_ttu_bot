from emup_ttu_bot.parsers import ScheduleParser, StationsParser

from urllib.parse import quote
import requests

URL = 'http://m.ettu.ru'
ALIASES = {
    "площадь": "пл",
    "проспект": "пр",
    "бульвар": "б-р",
    "проезд": "пр-д",
    "переулок": "пер",
    "станция": "ст",
    "гостиница": "гост",
    "семь": "7",
    "улица": "ул"  # вроде как не надо, но мало ли
}


class ETTUWrapper:
    def __init__(self):
        self._schedule_parser = ScheduleParser()
        self._stations_parser = StationsParser()

    def get_schedule_by_url(self, station_url: str):
        if URL not in station_url:
            station_url = URL + station_url
        schedule_raw = requests.get(station_url).text
        return self._schedule_parser.feed(schedule_raw)

    def _search_raw(self, first_letter: str, cleaned_query: str):
        stations_on_letter_raw = requests.get(
            URL + '/stations/' + quote(first_letter)).text
        raw_results = self._stations_parser.feed(stations_on_letter_raw)
        result_keys = raw_results.keys()
        results = {key: list(
                            filter(
                                lambda station:
                                cleaned_query in station[0].lower(),
                                raw_results[key])) for key in result_keys}
        return results

    def search_station(self, query: str):
        first_letter = query[:1].upper()
        cleaned_query = query.lower().replace('\n', '').strip()

        # обычный поиск
        results = self._search_raw(first_letter, cleaned_query)

        # поиск по алиасам
        for alias in ALIASES.keys():

            if alias in cleaned_query.split(' '):
                alias_results = self._search_raw(ALIASES[alias][:1].upper(),
                                                 ALIASES[alias])
                if len(cleaned_query.split(' ')) == 1:
                    for transport in alias_results.keys():
                        for station in alias_results[transport]:
                            results[transport].append(station)
                else:
                    for transport in alias_results.keys():
                        for station in alias_results[transport]:
                            for token in cleaned_query.split(' '):
                                if token in station[0].lower():
                                    results[transport].append(station)
        return results
